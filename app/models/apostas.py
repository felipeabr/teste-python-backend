import datetime
from app import db, ma

#Definição da classe / tabela usuário e seus campos

class Apostas(db.Model):
    id = db.Column(db.Integer, primary_key = True, autoincrement = True)
    id_user = db.Column(db.String(20), nullable=False)
    dezenas = db.Column(db.PickleType, nullable=False)
    ndezenas = db.Column(db.Integer)
    concurso = db.Column(db.Integer)
    realizada_em = db.Column(db.DateTime, default = datetime.datetime.now())

    def __init__(self,id_user,dezenas,ndezenas,concurso):
        self.id_user = id_user
        self.dezenas = dezenas
        self.ndezenas = ndezenas
        self.concurso = concurso

#Definindo o Schema do Marshmallow para facilitar a utilização de JSON
class ApostasSchema(ma.Schema):
    class Meta:
        fields = ('id','id_user','dezenas','ndezenas','concurso','realizada_em')

aposta_schema = ApostasSchema()
apostas_schema = ApostasSchema(many=True)

db.create_all()
db.session.commit()
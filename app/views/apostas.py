from werkzeug.security import generate_password_hash
from app import db
from flask import request, jsonify
from ..apis.loteria import Loteria
from ..models.apostas import Apostas, aposta_schema, apostas_schema
import random
from sqlalchemy import and_

#Função auxiliar para gerar dezenas aleatórias dentro de um intervalo específico
def gerarDezenas(n):
    return random.sample(range(1, 60), n)

#Função auziliar parar converter um array de strings para um array de integers
def converterArray(arr):
    for i in range(0, len(arr)):
        arr[i] = int(arr[i])
    return arr


#Função para cadastrar uma aposta, recebendo o id do usuário. Lembrando que opcionalmente pode receber uma relação das dezenas no JSON.
def post_aposta(id):
    ndezenas = request.json['ndezenas']
    try:
        dezenas = request.json['dezenas']
        dezenas.sort()
    except:
        dezenas = gerarDezenas(ndezenas)
        dezenas.sort()
    if ndezenas < 6 or ndezenas > 10:
        return jsonify({'message':'Quantidade de dezenas inválida.','data':{}}),500
    if len(dezenas) != ndezenas:
        return jsonify({'message':'O número de dezenas não está de acordo com a quantidade escolhida.','data':{}}),500
    concurso = int(Loteria().getConcurso())+1
    aposta = Apostas(id,dezenas, ndezenas,concurso)
    try:
        db.session.add(aposta)
        db.session.commit()
        result = aposta_schema.dump(aposta)
        return jsonify({'message':'Aposta cadastrada com sucesso!', 'data': result}),201
    except Exception as e:
        print(e)
        return jsonify({'message':'Erro ao criar aposta.','data':{}}),500


#Função para coletar o resultado da Mega Sena
def resultado_mega():
    mega = Loteria()
    try:
        return jsonify({'Concurso':mega.getConcurso(), 'Dezenas': converterArray(mega.getDezenas()), 'Data': mega.getDataconcurso(), 'Acumulado': mega.getAcumulado()}),200
    except Exception as e:
        print(e)
        return jsonify({'message':'Erro ao coletar os resultados.','data':{}}),500

#Função para conferir os acertos da última aposta em que o resultado já se encontra disponível na internet
def get_acertos_ultimo_jogo(id_u):
    proximoconcurso = int(Loteria().getConcurso())+1
    jogo = Apostas.query.filter(and_(Apostas.id_user == id_u,Apostas.concurso != proximoconcurso)).order_by(Apostas.id.desc()).first()
    if jogo:
        try:
            result = aposta_schema.dump(jogo)
            dezenassorteadas = converterArray(Loteria(str(result['concurso'])).getDezenas())
            dezenas_acertos = set(result['dezenas']) & set(dezenassorteadas)
            numero_de_acertos = len(dezenas_acertos)
            return jsonify({'message':'Dados coletados com sucesso.','ID da aposta':result['id'],'Concurso':result['concurso'],'Resultado do concurso':dezenassorteadas,'Sua aposta':result['dezenas'],'Numero de acertos':numero_de_acertos,'Dezenas corretas':list(dezenas_acertos)}), 200
        except:
            return jsonify({'message':'Erro ao coletar dados.'}), 500

#retornar todas as apostas realizadas pelo usuário
def get_jogos(id_u):
    apostas = Apostas.query.filter(Apostas.id_user == id_u).all()
    if apostas:
        result = apostas_schema.dump(apostas)
        return jsonify({'message':'Dados coletados com sucesso.','data':result}), 200
    return jsonify({'message':'Nenhum dado encontrado.','data':{}})

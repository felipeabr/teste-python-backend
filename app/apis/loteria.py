from urllib import request
from bs4 import BeautifulSoup 


def fetch(c):
    if(c!=""):
        url = "https://www.google.com/search?q=mega+sena+"+c
    else:
        url = "https://www.google.com/search?q=caixa+mega+sena"
    req = request.Request(url, data=None, headers={'User-Agent': 'Firefox/90.0'})
    page = request.urlopen(req)
    soup = BeautifulSoup(page,features="lxml")
    return soup


class Loteria:

    def __init__(self,c = ""):
        soup=fetch(c)
        self.dezenas = [s.text for s in soup.find_all('div',class_="MDTDab")[0].find_all('span')]
        dadosconcurso = soup.find('span',class_='qLLird').text
        self.concurso = int(dadosconcurso.split()[1])
        self.dataconcurso = dadosconcurso.split()[2].replace("(","").replace(")","")
        self.acumulado = soup.find('span',class_='br1ue').text.split()[1]

    def getDezenas(self):
        return self.dezenas

    def getConcurso(self):
        return self.concurso

    def getAcumulado(self):
        return self.acumulado

    def getDataconcurso(self):
        return self.dataconcurso

    
'''
loto = Loteria()


print(loto.getDezenas())
print(loto.getConcurso())
print(loto.getAcumulado())
print(loto.getDataconcurso())

loto = Loteria("2398")


print(loto.getDezenas())
print(loto.getConcurso())
print(loto.getAcumulado())
print(loto.getDataconcurso())
'''